package dyl.natalia.caesar.alphabets;

import java.nio.CharBuffer;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Alphabet {

	private final char[] lettersInOrder;
	private final char mostFrequentLetter;
	
	public Alphabet(String alphabet, char mostFrequentLetter) {
		if(checkAlphabet(alphabet)) {
			throw new IllegalArgumentException("Incorrect alphabet.");
		}
		this.lettersInOrder = alphabet.toUpperCase().toCharArray();
		
		if(!isFromAlphabet(mostFrequentLetter)) {
			throw new IllegalArgumentException("Incorrect most frequent letter.");
		}
		this.mostFrequentLetter = Character.toUpperCase(mostFrequentLetter);
	}
	
	private boolean isCorrectAlphabet(String alphabet) {
		return alphabet
				.chars()
				.allMatch(ch -> Character.isAlphabetic(ch));
	}
	
	private boolean checkAlphabet(String alphabet) {
		return !isCorrectAlphabet(alphabet) || alphabet.isEmpty();
	}

	public boolean isFromAlphabet(char letterToCheck) {
		return CharBuffer
				.wrap(lettersInOrder)
				.chars()
				.anyMatch(letter -> Character.toUpperCase(letterToCheck) == letter);
	}

	public int getMostFrequentLetterIndex() {
		return getIndexOfLetter(mostFrequentLetter);
	}
	
	public int getLength() {
		return lettersInOrder.length;
	}
	
	public char getLetterAtIndex(int index) {
		return lettersInOrder[index];
	}
	
	public int getIndexOfLetter(char letterToFind) {
		OptionalInt idx = IntStream.range(0, lettersInOrder.length)
				.filter(i -> lettersInOrder[i] == Character.toUpperCase(letterToFind))
				.findAny();
		
		if(!idx.isPresent()) {
			return -1;
		}
		
		return idx.getAsInt();
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == this) return true;
		if(!(other instanceof Alphabet)) return false;
		
		Alphabet alphabet = (Alphabet) other;
		String thisLetters = String.valueOf(lettersInOrder);
		String otherLetters = String.valueOf(alphabet.lettersInOrder);
		
		return otherLetters.equals(thisLetters)
				&& alphabet.mostFrequentLetter == mostFrequentLetter;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		String letters = String.valueOf(lettersInOrder);
		result = 31 * result + letters.hashCode();
		result = 31 * result + Character.hashCode(mostFrequentLetter);
		return result;
	}
	
}
