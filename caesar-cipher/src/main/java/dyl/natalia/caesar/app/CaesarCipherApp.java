package dyl.natalia.caesar.app;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import dyl.natalia.caesar.alphabets.Alphabet;
import dyl.natalia.caesar.helpers.AlphabetCreator;
import dyl.natalia.caesar.helpers.FileManager;
import dyl.natalia.caesar.logic.CaesarCipher;

public class CaesarCipherApp {

	public static void main(String[] args) {
		if(!checkArgsLength(args.length) || !checkFirstArg(args[0])) {
			System.out.println(showUsage());
		}
		String toReadFilename = args[1];
		String language = "english";
		if(isLanguage(args[1]) && args.length != 2) {
			language = args[1].toLowerCase();
			toReadFilename = args[2];
		}
		Alphabet alphabet = getAlphabet(language);
		CaesarCipher caesarCipher = new CaesarCipher(alphabet);

		String message = readFile(toReadFilename);

		String action = args[0].toLowerCase();
		String changedMessage = executeAction(action, caesarCipher, message);		

		String writeToFile = getWriteToFile(args);
		FileManager.writeToFile(writeToFile, changedMessage);	
	}

	private static String getWriteToFile(String[] args) {
		String defaultName = "out";
		if (args.length == 3 && !isLanguage(args[1])) {
			return args[2];
		} else if (args.length == 4) {
			return args[3];
		}
		return defaultName;
	}

	private static boolean isLanguage(String language) {
		Map<String, Alphabet> languages = AlphabetCreator.getAllAlphabets();
		return languages.containsKey(language);
	}

	private static String showUsage() {
		String usage = "SYNTAX \nenc|encrypt|dec|decrypt [language] in [out]"
				+ "\nArgs in square brackets have default values:"
				+ "\nlanguage is 'english'"
				+ "\nout is 'out'";
		return usage;
	}

	private static boolean checkArgsLength(int argsLength) {
		return argsLength <= 4 || argsLength >=2;
	}

	private static boolean checkFirstArg(String firstArg) {
		return firstArg.matches("enc|encrypt|dec|decrypt");
	}

	private static Alphabet getAlphabet(String language) {
		Optional<Alphabet> alphabet = AlphabetCreator.getAlphabet(language);
		if(!alphabet.isPresent()) {
			System.out.println("Available languages: ");
			Iterator<String> alphabetNames = AlphabetCreator.getAllAlphabets().keySet().iterator();
			alphabetNames.forEachRemaining(i -> System.out.println(i));
		}

		return alphabet.get();
	}

	private static int getShift() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter shift value: ");
		int shift = sc.nextInt();
		sc.close();

		return shift;
	}

	private static String readFile(String toReadFilename) {
		String message = FileManager.fileContentToString(toReadFilename);
		if(message.isEmpty()) {
			System.out.println("File to read doesn't exist");
			System.exit(0);
		}

		return message;
	}

	private static String executeAction(String action, CaesarCipher caesarCipher, String message) {
		String changedMessage = null;
		if(action.matches("enc|encrypt")) {
			int shift = getShift();
			changedMessage = caesarCipher.encrypt(message, shift);
		} else {
			changedMessage = caesarCipher.decrypt(message);
		}

		return changedMessage;
	}
}
