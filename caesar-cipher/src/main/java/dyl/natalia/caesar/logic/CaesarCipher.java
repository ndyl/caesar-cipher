package dyl.natalia.caesar.logic;

import java.util.HashMap;
import java.util.Map;

import dyl.natalia.caesar.alphabets.Alphabet;

public class CaesarCipher {
	private Alphabet alphabet;

	public CaesarCipher(Alphabet alphabet) {
		this.alphabet = alphabet;
	}

	public String encrypt(String message, int shift) {
		char[] charsFromMessage = message.toCharArray();
		StringBuilder encrypted = new StringBuilder(message.length());
		for(char ch : charsFromMessage) {
			if(isLetterFromCurrentAlphabet(ch)) {
				char shiftedChar = shiftChar(ch, shift);
				encrypted.append(shiftedChar);
			} else {
				encrypted.append(ch);
			}
		}
		String encryptedMessage = encrypted.toString();
		return encryptedMessage;
	}

	private char shiftChar(char ch, int shift) {
		int charInAlphabetIdx = alphabet.getIndexOfLetter(ch);
		int shiftedCharIdx = charInAlphabetIdx + shift;
		int alphabetLength = alphabet.getLength();
		shiftedCharIdx = shiftedCharIdx % alphabetLength;
		if(shiftedCharIdx < 0) {
			shiftedCharIdx += alphabetLength;
		}

		char shiftedChar = alphabet.getLetterAtIndex(shiftedCharIdx);
		if(Character.isLowerCase(ch)) {
			shiftedChar = Character.toLowerCase(shiftedChar);
		}

		return shiftedChar;
	}

	public String decrypt(String encryptedMessage) {
		int mostFrequentLetterInAlphabetIdx = alphabet.getMostFrequentLetterIndex();
		int mostFrequentLetterInTextIdx = getMostFrequentLetterInTextIdx(encryptedMessage);

		int shift = 0;

		if(mostFrequentLetterInTextIdx < mostFrequentLetterInAlphabetIdx) {
			shift = mostFrequentLetterInAlphabetIdx - mostFrequentLetterInTextIdx;
		} else {
			shift = alphabet.getLength() - (mostFrequentLetterInTextIdx - mostFrequentLetterInAlphabetIdx);
		}

		String decryptedMessage = encrypt(encryptedMessage, shift);

		return decryptedMessage;
	}

	private int getMostFrequentLetterInTextIdx(String text) {
		Map<Character, Integer> lettersFrequency = getLettersFrequencies(text);
		int mostFrequentLetterIdx = -1;

		Character mostFrequentLetter = lettersFrequency
				.entrySet()
				.stream()
				.max(Map.Entry.comparingByValue(Integer::compareTo))
				.get()
				.getKey();
		mostFrequentLetterIdx = alphabet.getIndexOfLetter(mostFrequentLetter);
		return mostFrequentLetterIdx;
	}

	private HashMap<Character, Integer> getLettersFrequencies(String text) {
		Map<Character, Integer> lettersFrequency = new HashMap<Character, Integer>();
		for(char ch : text.toUpperCase().toCharArray()) {
			if(isLetterFromCurrentAlphabet(ch)) {
				if(lettersFrequency.containsKey(ch)) {
					int currentValue = lettersFrequency.get(ch);
					lettersFrequency.put(ch, currentValue + 1);
				} else {
					lettersFrequency.put(ch, 1);
				}
			}
		}
		
		return (HashMap<Character, Integer>) lettersFrequency;
	}

	private boolean isLetterFromCurrentAlphabet(char ch) {
		return alphabet.isFromAlphabet(ch);
	}

	public void changeAlphabet(Alphabet newAlphabet) {
		this.alphabet = newAlphabet;
	}

}
