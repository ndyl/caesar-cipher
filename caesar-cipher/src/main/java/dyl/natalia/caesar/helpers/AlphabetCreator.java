package dyl.natalia.caesar.helpers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import dyl.natalia.caesar.alphabets.Alphabet;

public class AlphabetCreator {
	private AlphabetCreator() {
		
	}
	
	public static Optional<Alphabet> getAlphabet(String alphabetName) {
		Alphabet alphabet = null;
		if(!isLanguageAvailable(alphabetName)) {
			return Optional.ofNullable(alphabet);
		} 
		
		Map<String, Alphabet> alphabets = getAllAlphabets();
		alphabet = alphabets.get(alphabetName);
		return Optional.of(alphabet);
		
	}
	
	public static Alphabet getEnglishAlphabet() {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		char mostFrequentLetter = 'e';
		Alphabet english = new Alphabet(alphabet, mostFrequentLetter);
		return english;
	}
	
	public static Alphabet getPolishAlphabet() {
		String alphabet = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż";
		char mostFrequentLetter = 'a';
		Alphabet polish = new Alphabet(alphabet, mostFrequentLetter);
		return polish;
	}
	
	public static Alphabet getSwedishAlphabet() {
		String alphabet = "abcdefghijklmnopqrstuvwxyzåäö";
		char mostFrequentLetter = 'e';
		Alphabet swedish = new Alphabet(alphabet, mostFrequentLetter);
		return swedish;
	}
	
	public static Map<String, Alphabet> getAllAlphabets() {
		Map<String, Alphabet> alphabets = new HashMap<String, Alphabet>();
		alphabets.put("english", getEnglishAlphabet());
		alphabets.put("polish", getPolishAlphabet());
		alphabets.put("swedish", getSwedishAlphabet());
			
		return Collections.unmodifiableMap(alphabets);
	}
	
	private static boolean isLanguageAvailable(String language) {
		String langLowerCase = language.toLowerCase();
		Map<String, Alphabet> alphabets = getAllAlphabets();
		return alphabets.containsKey(langLowerCase);
	}

}
