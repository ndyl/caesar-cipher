package dyl.natalia.caesar.helpers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileManager {
	private static final String MESSAGES_PATH = "../messages/";
	
	private FileManager() {

	}

	public static String fileContentToString(String filename) {
		StringBuilder fileContentBuilder = new StringBuilder();
		Path pathToFile = Paths.get(MESSAGES_PATH+filename);
		
		if(!Files.exists(pathToFile)) {
			return "";
		}
		
		try (Stream<String> textStream = Files.lines(pathToFile)) {
			textStream
						.forEach(i -> {
							fileContentBuilder.append(i);
							fileContentBuilder.append('\n');
						});
		} catch (IOException e) {
			e.printStackTrace();
		} 

		return fileContentBuilder.toString();
	}

	public static void writeToFile(String filename, String fileContent) {
		Path path = Paths.get(MESSAGES_PATH+filename);
		String[] lines = fileContent.split("\n");
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			for(String line : lines) {
				writer.write(line);
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
