package dyl.natalia.caesar.alphabets;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AlphabetTest {
	
	private Alphabet alphabet;
	private String letters = "abcdefgh";
	private char mostFrequentLetter = 'a';
	
	@Before
	public void setUp() {
		alphabet = new Alphabet(letters, mostFrequentLetter);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_givenIncorrectLettersAndMostFrequentLetter_shouldThrowIllegalArgumentException() {
		String letters = "123456*.x";
		char mostFrequentLetter = 'a';
		new Alphabet(letters, mostFrequentLetter);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_givenIncorrectMostFrequentLetter_shouldThrowIllegalArgumentException() {
		String letters = this.letters;
		char mostFrequentLetter = 'z';
		new Alphabet(letters, mostFrequentLetter);
	}
	
	@Test
	public void isFromAlphabet_givenIncorrectChar_shouldReturnFalse() {
		assertFalse(alphabet.isFromAlphabet('z'));
	}
	
	@Test
	public void isFromAlphabet_givenUpperCaseCorrectChar_shouldReturnTrue() {
		assertTrue(alphabet.isFromAlphabet('A'));
	}
	
	@Test
	public void getMostFrequentLetterIndex_shouldReturnZero() {
		int result = alphabet.getMostFrequentLetterIndex();
		assertEquals(0, result);
	}
	
	@Test
	public void getLength_shouldReturn8() {
		int result = alphabet.getLength();
		assertEquals(8, result);
	}
	
	@Test
	public void getLetterAtIndex_givenZero_shouldReturnH() {
		char result = alphabet.getLetterAtIndex(7);
		assertEquals('H', result);
	}
	
	@Test
	public void getIndexOfLetter_givenD_shouldReturn3() {
		int result = alphabet.getIndexOfLetter('D');
		assertEquals(3, result);
	}
	
	@Test
	public void getIndexOfLetter_givenLowerCasedD_shouldReturn3() {
		int result = alphabet.getIndexOfLetter('d');
		assertEquals(3, result);
	}
	
	@Test
	public void getIndexOfLetter_givenN_shoulReturnNegative1() {
		int result = alphabet.getIndexOfLetter('N');
		assertEquals(-1, result);
	}
}