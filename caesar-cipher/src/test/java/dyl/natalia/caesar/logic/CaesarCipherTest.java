package dyl.natalia.caesar.logic;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import dyl.natalia.caesar.alphabets.Alphabet;

public class CaesarCipherTest {
	
	private String letters = "aelp";
	private CaesarCipher caesarCipher;
	private String message;
	private int shift;
	
	@Before
	public void setUp() {
		final Alphabet alphabet = mock(Alphabet.class, withSettings()
				.useConstructor(letters, 'p')
				.defaultAnswer(CALLS_REAL_METHODS));
		caesarCipher = new CaesarCipher(alphabet);
		message = "apple";
		shift = 2;
	}
	
	@Test
	public void encrypt_messageContainsOnlyLettersFromAlphabet_shouldEncryptWholeMessage() {
		String encrypted = caesarCipher.encrypt(message, shift);
		assertEquals("leeap", encrypted);
	}

	@Test
	public void encrypt_messageContainsCharsNotFromAlphabet_shouldEncryptOnlyLettersFromAlphabet() {
		message = "a.p.ples12";
		String expected = "l.e.eaps12";
		String encrypted = caesarCipher.encrypt(message, shift);
		assertEquals(expected, encrypted);
	}
	
	@Test
	public void encrypt_givenAlphabetLen4Shift25_shouldEncryptWithShift1() {
		shift = 25;
		String encryptedWith25 = caesarCipher.encrypt(message, shift);
		shift = 1;
		String encryptedWith1 = caesarCipher.encrypt(message, shift);
		String expected = "eaapl";
		assertEquals(expected, encryptedWith25);
		assertEquals(encryptedWith25, encryptedWith1);
	}
	
	@Test
	public void encrypt_givenShiftNegative1_shouldEncryptWithShift3() {
		shift = -1;
		String encryptedWithNeg1 = caesarCipher.encrypt(message, shift);
		shift = 3;
		String encryptedWith3 = caesarCipher.encrypt(message, shift);
		String expected = "pllea";
		assertEquals(expected, encryptedWithNeg1);
		assertEquals(encryptedWithNeg1, encryptedWith3);
	}
	
	@Test
	public void encrypt_givenShiftNegative13_shouldEncryptWithShift3() {
		shift = -13;
		String encryptedWithNeg13 = caesarCipher.encrypt(message, shift);
		shift = 3;
		String encryptedWith3 = caesarCipher.encrypt(message, shift);
		String expected = "pllea";
		assertEquals(expected, encryptedWithNeg13);
		assertEquals(encryptedWithNeg13, encryptedWith3);
	}

	@Test
	public void decrypt_givenEncryptedMessageWithShift3_shouldDecryptToApple() {
		message = "pllea";
		String decrypted = caesarCipher.decrypt(message);
		String expected = "apple";
		assertEquals(expected, decrypted);
	}
}
