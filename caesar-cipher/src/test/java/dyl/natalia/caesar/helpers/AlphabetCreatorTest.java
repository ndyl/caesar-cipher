package dyl.natalia.caesar.helpers;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Test;

import dyl.natalia.caesar.alphabets.Alphabet;

public class AlphabetCreatorTest {
	
	@Test
	public void getAlphabet_givenEnglish_shouldReturnEnglishAlphabet() {
		String alphabetName = "english";
		Alphabet english = AlphabetCreator.getAlphabet(alphabetName).get();
		assertTrue(english.equals(AlphabetCreator.getEnglishAlphabet()));
	}
	
	@Test
	public void getAlphabet_givenPolish_shouldReturnPolishAlphabet() {
		String alphabetName = "polish";
		Alphabet polish = AlphabetCreator.getAlphabet(alphabetName).get();
		assertTrue(polish.equals(AlphabetCreator.getPolishAlphabet()));
	}
	
	@Test
	public void getAlphabet_givenSwedish_shouldReturnSwedishAlphabet() {
		String alphabetName = "swedish";
		Alphabet swedish = AlphabetCreator.getAlphabet(alphabetName).get();
		assertTrue(swedish.equals(AlphabetCreator.getSwedishAlphabet()));
	}
	
	@Test
	public void getAlphabet_givenIncorrectAlphabet_shouldReturnOptionalWithoutValue() {
		String alphabetName = "abcde";
		Optional<Alphabet> alphabet = AlphabetCreator.getAlphabet(alphabetName);
		assertFalse(alphabet.isPresent());
	}
	
	@Test
	public void getAllAlphabets_given3Languages_shouldReturnMapOfSize3() {
		int alphabetsNumber = AlphabetCreator.getAllAlphabets().size();
		int expectedSize = 3;
		assertEquals(expectedSize, alphabetsNumber);
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void getAllAlphabets_tryAddSomeValues_shouldThrowUnsupportedOperationException() {
		Alphabet newAlphabet = new Alphabet("abcdefgh", 'c');
		AlphabetCreator.getAllAlphabets().put("new alphabet", newAlphabet);
	}

}
