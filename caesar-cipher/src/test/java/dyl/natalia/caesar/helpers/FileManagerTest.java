package dyl.natalia.caesar.helpers;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

public class FileManagerTest {
	private final String messagesPath = "../messages/";
	
	@Test
	public void fileContentToString_givenCorrectFilename_shouldReturnEmptyString() {
		String nonExistingFilename = "caesar";
		String result = FileManager.fileContentToString(nonExistingFilename);
		assertTrue(result.isEmpty());
	}
	
	@Test
	public void fileContentToString_givenCorrectFilename_shouldReturnNonEmptyString() {
		String existingFilename = "en_message.txt";
		String result = FileManager.fileContentToString(existingFilename);
		assertFalse(result.isEmpty());
	}
	
	@Test
	public void writeToFile_givenNonExistingFileName_shouldCreateFileWithThisName() throws IOException {
		String nonExistingFilename = "aaa";
		String content = "";
		assertFalse(Files.exists(Paths.get(messagesPath+nonExistingFilename)));
		FileManager.writeToFile(nonExistingFilename, content);
		assertTrue(Files.exists(Paths.get(messagesPath+nonExistingFilename)));
		
		Files.delete(Paths.get(messagesPath+nonExistingFilename));
	}
	
}
