# README #

### Caesar Cipher Java implementation ###

Implementation of well-known cipher.  
With this application it is possible to encrypt / decrypt messages in some languages.

### Syntax ###

`enc|encrypt|dec|decrypt [language] in [out]`

language - one of available languages (English, Polish, Swedish)  
in|out - name of file

For args in square brackets default values are provided:  
language = 'english'  
out = 'out'

### Important note ###

Decryption use frequency analysis, so not every text will be decrypted properly. Use messages longer than few sentences.